<?php

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24TransactionInterface;

return static function (DefinitionConfigurator $definition) {
    $definition->rootNode()
            ->children()
                ->arrayNode('entities')
                    ->isRequired()
                    ->children()
                        ->scalarNode('transaction')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->validate()
                            ->ifTrue(fn ($value) => !is_a($value, Przelewy24TransactionInterface::class, true))
                            ->thenInvalid(sprintf('Class %%s does not implement the %s.', Przelewy24TransactionInterface::class))
                        ->end()
                    ->end()
                ->end()
            ->end()
                ->arrayNode('api')
                    ->isRequired()
                    ->children()
                        ->scalarNode('merchant_id')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('reports_key')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('crc')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->booleanNode('is_live')
                            ->isRequired()
                        ->end()
                    ->end()
                ->end()
            ->end()
    ;
};
