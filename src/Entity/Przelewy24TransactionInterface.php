<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Entity;

use Symfony\Component\Uid\Uuid;
use XOne\Bundle\Przelewy24Bundle\Enum\Przelewy24TransactionStatus;

interface Przelewy24TransactionInterface
{
    public function getId(): ?Uuid;

    public function getStatus(): ?Przelewy24TransactionStatus;

    public function setStatus(Przelewy24TransactionStatus $status): void;

    public function getAmount(): ?int;

    public function setAmount(int $amount): static;

    public function getDescription(): ?string;

    public function setDescription(string $description): static;

    public function getEmail(): ?string;

    public function setEmail(string $email): static;

    public function getUrlReturn(): ?string;

    public function setUrlReturn(string $urlReturn): static;

    public function getToken(): ?string;

    public function setToken(?string $token): static;

    public function getUrlGateway(): ?string;

    public function setUrlGateway(?string $urlGateway): static;

    public function getOrderId(): ?string;

    public function setOrderId(?string $orderId): static;
}
