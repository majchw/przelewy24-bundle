<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Entity;

use XOne\Bundle\Przelewy24Bundle\Enum\Przelewy24TransactionStatus;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[MappedSuperclass]
class Przelewy24Transaction implements Przelewy24TransactionInterface
{
    // todo: validation

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['przelewy24'])]
    protected ?Uuid $id = null;

    #[ORM\Column(options: ['default' => Przelewy24TransactionStatus::UNSUBMITTED->value])]
    protected ?int $status = Przelewy24TransactionStatus::UNSUBMITTED->value;

    #[ORM\Column]
    protected ?int $amount = null;

    #[ORM\Column(length: 255)]
    protected ?string $description = null;

    #[ORM\Column(length: 255)]
    protected ?string $email = null;

    #[ORM\Column(length: 255)]
    protected ?string $urlReturn = null;

    #[ORM\Column(length: 255, nullable: true)]
    protected ?string $token = null;

    #[ORM\Column(length: 255, nullable: true)]
    protected ?string $urlGateway = null;

    #[ORM\Column(nullable: true)]
    protected ?string $orderId = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getStatus(): ?Przelewy24TransactionStatus
    {
        return is_null($this->status) ? null : Przelewy24TransactionStatus::from($this->status);
    }

    /** @internal Status is manipulated internally inside Transaction handling code. */
    public function setStatus(Przelewy24TransactionStatus $status): void
    {
        $this->status = $status->value;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /** Amount is an integer - pass the amount of lowest denomination.
     *
     * For `1.25 PLN`, `amount` equals `125`
     */
    public function setAmount(int $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /** Mandatory transaction description. */
    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getUrlReturn(): ?string
    {
        return $this->urlReturn;
    }

    /** Please provide an absolute URL: https://website.com/relative-path instead of /relative-path.
     *
     * The client will be redirected to this URL after a successful payment. It can lead to another domain.
     */
    public function setUrlReturn(string $urlReturn): static
    {
        $this->urlReturn = $urlReturn;

        return $this;
    }

    /** Token is available after successfully saving the transaction vio Przelewy24 API. */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /** @internal Token will be set after API submission */
    public function setToken(?string $token): static
    {
        $this->token = $token;

        return $this;
    }

    /** Gateway URL is available after successfully saving the transaction vio Przelewy24 API. */
    public function getUrlGateway(): ?string
    {
        return $this->urlGateway;
    }

    /** @internal Gateway URL will be set after API submission */
    public function setUrlGateway(?string $urlGateway): static
    {
        $this->urlGateway = $urlGateway;

        return $this;
    }

    /** Order ID is available after successful payment confirmation from Przelewy24. */
    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    /** @internal Order ID will be set after webhook call. */
    public function setOrderId(?string $orderId): static
    {
        $this->orderId = $orderId;

        return $this;
    }
}
