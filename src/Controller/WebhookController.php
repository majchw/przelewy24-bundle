<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;
use XOne\Bundle\Przelewy24Bundle\Factory\Przelewy24ClientFactory;
use XOne\Bundle\Przelewy24Bundle\Service\TransactionVerifyService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('_przelewy24', name: 'x_one_przelewy24_')]
class WebhookController extends AbstractController
{
    public function __construct(
        protected TranslatorInterface $translator,
    ) {}

    #[Route('/status-webhook', name: 'status_webhook', methods: ['POST'])]
    public function statusWebhook(
        Przelewy24ClientFactory $clientFactory,
        TransactionVerifyService $verifyService,
        LoggerInterface $logger,
    ): Response {
        $logger->debug('Przelewy24 Status webhook has been called');

        $przelewy24 = $clientFactory->create();
        // TODO: Use Request instead of php://input to improve testability
        // TODO: Handle webhook in a separate service & create DTO for its result
        $webhook = $przelewy24->handleWebhook(json_decode(file_get_contents('php://input'), true));
        $logger->info('Przelewy24 Status webhook has received data: '.var_export($webhook, true));

        try {
            $session = $webhook->sessionId();
            $order = $webhook->orderId();
            $amount = $webhook->amount();
        } catch (\Throwable) {
            $message = $this->translator->trans(
                'error.api.invalid_payload',
                [],
                'x_one_przelewy24',
            );
            $logger->log('error', $message);
            return $this->json(['code' => 400, 'message' => $message], 400);
        }

        $transaction = $verifyService->verifyTransaction($session, $order, $amount);
        if (!$transaction) {
            $message = $this->translator->trans(
                'error.api.verification_failed',
                [],
                'x_one_przelewy24',
            );
            $logger->log('error', $message);
            return $this->json(['code' => 400, 'message' => $message], 400);
        }

        return $this->json(['code' => 200]);
    }
}
