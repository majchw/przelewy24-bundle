<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24TransactionInterface;
use XOne\Bundle\Przelewy24Bundle\Factory\Przelewy24ClientFactory;
use XOne\Bundle\Przelewy24Bundle\Factory\TransactionRepositoryFactory;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class XOnePrzelewy24Bundle extends AbstractBundle
{
    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $config = $builder->getExtensionConfig('x_one_przelewy24')[0];

        $container->extension('doctrine', [
            'orm' => [
                'resolve_target_entities' => array_filter([
                    Przelewy24TransactionInterface::class => $config['entities']['transaction'] ?? null,
                ]),
            ],
        ]);
    }

    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import('../config/services.yaml');

        $container->parameters()
            ->set('x_one_przelewy24.entity.transaction.class', $config['entities']['transaction'])
        ;

        $container->services()
            ->set(TransactionRepositoryFactory::class)
            ->args([
                service(EntityManagerInterface::class),
                param('x_one_przelewy24.entity.transaction.class')
            ])
            ->set(Przelewy24ClientFactory::class)
            ->args([
                $config['api']['merchant_id'],
                $config['api']['reports_key'],
                $config['api']['crc'],
                $config['api']['is_live']
            ])
        ;
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->import('../config/definition.php');
    }
}
