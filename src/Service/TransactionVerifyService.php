<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Service;

use Symfony\Contracts\Translation\TranslatorInterface;
use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24TransactionInterface;
use XOne\Bundle\Przelewy24Bundle\Enum\Przelewy24TransactionStatus;
use XOne\Bundle\Przelewy24Bundle\Factory\Przelewy24ClientFactory;
use XOne\Bundle\Przelewy24Bundle\Factory\TransactionRepositoryFactory;
use XOne\Bundle\Przelewy24Bundle\Messenger\Przelewy24TransactionPaid;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/** @internal
 * @todo verifyTransaction - Move [session, order, amount] into a DTO representing Przelewy24 API response
 */
class TransactionVerifyService
{
    public function __construct(
        protected TransactionRepositoryFactory $repositoryFactory,
        protected Przelewy24ClientFactory $przelewy24ClientFactory,
        protected LoggerInterface $logger,
        protected EntityManagerInterface $entityManager,
        protected MessageBusInterface $messageBus,
        protected TranslatorInterface $translator,
    ) {
    }

    /**
     * Calls Przelewy24 API to determine if the given transaction has been paid.
     *
     * **Flushes the entity manager!**
     *
     * @param string $session Received from Przelewy24 API webhook
     * @param int    $order   Received from Przelewy24 API webhook
     * @param int    $amount  Received from Przelewy24 API webhook
     *
     * @return ?Przelewy24TransactionInterface Updated Przelewy24Transaction on success
     *
     * @internal
     */
    public function verifyTransaction(
        string $session,
        int $order,
        int $amount,
    ): ?Przelewy24TransactionInterface {
        $repository = $this->repositoryFactory->getRepository();
        /** @var ?Przelewy24TransactionInterface $transaction */
        $transaction = $repository->find($session);

        if (is_null($transaction)) {
            $message = $this->translator->trans(
                'error.transaction.not_found',
                ['%id%' => $session],
                'x_one_przelewy24',
            );
            throw new \Exception($message);
        }

        if (Przelewy24TransactionStatus::SUBMITTED !== $transaction->getStatus()) {
            // Prevent a race condition if the programmer forgot to flush the entity manager.
            // Otherwise, the status could be changed to ::PAID, then back to ::SUBMITTED
            // This way, if a programmer error happens, the transaction is locked in "Waiting" status,
            //   errors will occur in logs and funds will not be transferred to the shop
            $message = $this->translator->trans(
                'error.transaction.not_submitted',
                [],
                'x_one_przelewy24',
            );
            throw new \Exception($message);
        }

        if ($transaction->getAmount() !== $amount) {
            $message = $this->translator->trans(
                'error.api.amount_mismatch',
                [],
                'x_one_przelewy24',
            );
            throw new \Exception($message);
        }

        $przelewy24 = $this->przelewy24ClientFactory->create();
        $response = $przelewy24->transactions()->verify(
            sessionId: $session,
            orderId: $order,
            amount: $amount,
        );

        $this->logger->info('Przelewy24 Verification has returned data: '.json_encode($response->parameters()));

        $successful = $response->parameters()['data']['status'] ?? null === 'success';
        if (!$successful) {
            $this->logger->warning(
                'Received invalid verification response from Przelewy24 API for ID '
                .$transaction->getId()
                .': '.json_encode($response->parameters())
            );

            return null;
        }

        $this->logger->info('Przelewy24 Verification for transaction ID '.$transaction->getId().' is successful!');

        try {
            $transaction->setStatus(Przelewy24TransactionStatus::PAID);
            $transaction->setOrderId((string) $order);

            // Makes the service more complex, but helps with the common use-case
            $this->entityManager->flush();
        } catch (\Throwable $e) {
            $this->logger->critical('Updating the Przelewy24Transaction status has failed on successful payment!: '.$e);
            throw $e;
        }

        $this->messageBus->dispatch(new Przelewy24TransactionPaid(
            transactionId: $transaction->getId(),
            transactionFQCN: get_class($transaction),
        ));

        return $transaction;
    }
}
