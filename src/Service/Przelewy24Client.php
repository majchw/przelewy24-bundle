<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Service;

use ApiPlatform\Api\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24TransactionInterface;
use XOne\Bundle\Przelewy24Bundle\Enum\Przelewy24TransactionStatus;
use XOne\Bundle\Przelewy24Bundle\Factory\Przelewy24ClientFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @todo Support other API fields - Currency, language, ...
 */
class Przelewy24Client
{
    public function __construct(
        protected ValidatorInterface $validator,
        protected Przelewy24ClientFactory $clientFactory,
        protected LoggerInterface $logger,
        protected EntityManagerInterface $entityManager,
        protected UrlGeneratorInterface $urlGenerator,
        protected TranslatorInterface $translator,
    ) {
    }

    /** Submits a Transaction to Przelewy24.
     *
     * If true, you will be able to read from the `$transaction->getUrlGateway()` and `$transaction->getToken()` methods.
     *
     * **Flushes the entity manager!**
     *
     * @return bool true if the call was successful
     */
    public function submitTransaction(Przelewy24TransactionInterface $transaction): bool
    {
        $this->validateTransaction($transaction);

        try {
            $przelewy24 = $this->clientFactory->create();

            $response = $przelewy24->transactions()->register(
                // Use Transaction.id as sessionId, as UUIDv4 gives us enough randomness,
                // and it is simpler than creating a unique ID
                sessionId: $transaction->getId()->toRfc4122(),
                amount: $transaction->getAmount(),
                description: $transaction->getDescription(),
                email: $transaction->getEmail(),
                urlReturn: $transaction->getUrlReturn(),

                // All transactions can be routed to the same controller
                urlStatus: $this->getUrlStatus(),

                // Unlimited time to complete the transaction,
                // in case the user wants to access it via email for ex. after 2 hours
                timeLimit: 0
            );

            $transaction->setToken($response->token());
            $transaction->setUrlGateway($response->gatewayUrl());
            $transaction->setStatus(Przelewy24TransactionStatus::SUBMITTED);
        } catch (\Throwable $e) {
            $this->logger->error('Unable to call or read from the Przelewy24 API: '.$e);
            $transaction->setStatus(Przelewy24TransactionStatus::SUBMISSION_FAILED);
        }

        // Makes the service more complex, but helps with the common use-case
        $this->entityManager->flush();

        return Przelewy24TransactionStatus::SUBMITTED === $transaction->getStatus();
    }

    protected function validateTransaction($transaction): void
    {
        if (is_null($transaction->getId())) {
            $message = $this->translator->trans(
                'error.transaction.not_saved',
                [],
                'x_one_przelewy24',
            );
            throw new \InvalidArgumentException($message);
            // Prevents race conditions - if we wouldn't save the Transaction to db prior to calling API,
            // then a webhook could theoretically fire before the entity is saved, and data would be lost
        }

        $validationResult = $this->validator->validate($transaction);
        if ($validationResult->count() > 0) {
            $message = $this->translator->trans(
                'error.transaction.validation',
                ['%error%' => $validationResult],
                'x_one_przelewy24',
            );
            throw new \InvalidArgumentException($message);
        }

        if (!in_array($transaction->getStatus(), [
            Przelewy24TransactionStatus::UNSUBMITTED,
            Przelewy24TransactionStatus::SUBMISSION_FAILED,
        ])) {
            $message = $this->translator->trans(
                'error.transaction.invalid_status_for_submission',
                [],
                'x_one_przelewy24',
            );
            throw new \InvalidArgumentException($message);
        }
    }

    protected function getUrlStatus(): string
    {
        return $this->urlGenerator->generate('x_one_przelewy24_status_webhook', [], UrlGenerator::ABSOLUTE_URL);
    }
}
