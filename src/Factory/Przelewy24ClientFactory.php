<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Factory;

use Przelewy24\Przelewy24;

/** @internal
 */
class Przelewy24ClientFactory
{
    public function __construct(
        private int $przelewy24MerchantId,
        private string $przelewy24ReportsKey,
        private string $przelewy24Crc,
        private bool $przelewy24isLive,
    ) {
    }

    public function create(): Przelewy24
    {
        return new Przelewy24(
            merchantId: $this->przelewy24MerchantId,
            reportsKey: $this->przelewy24ReportsKey,
            crc: $this->przelewy24Crc,
            isLive: $this->przelewy24isLive,
        );
    }
}
