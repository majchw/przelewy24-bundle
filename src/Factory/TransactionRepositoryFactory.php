<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24TransactionInterface;

/** @internal
 */
class TransactionRepositoryFactory
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private string $transactionEntityFqcn,
    ) {
    }

    /** @return ServiceEntityRepository<Przelewy24TransactionInterface> */
    public function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->transactionEntityFqcn);
    }
}
