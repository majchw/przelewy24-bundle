<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Messenger;

use Symfony\Component\Uid\Uuid;

readonly class Przelewy24TransactionPaid
{
    public function __construct(
        public Uuid $transactionId,
        public string $transactionFQCN,
    ) {
    }
}
