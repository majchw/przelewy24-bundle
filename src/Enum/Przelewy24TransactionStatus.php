<?php

declare(strict_types=1);

namespace XOne\Bundle\Przelewy24Bundle\Enum;

enum Przelewy24TransactionStatus: int
{
    /** The transaction hasn't been yet submitted to Przelewy24 API. */
    case UNSUBMITTED = 0;

    /** The transaction has been submitted to Przelewy24 API, but it has failed. Resubmission is possible. */
    case SUBMISSION_FAILED = 1;

    /** The transaction has been successfully submitted to Przelewy24 API and is awaiting payment. */
    case SUBMITTED = 2;

    /** The transaction has received confirmation from Przelewy24 that it has been fully paid. */
    case PAID = 3;
}
