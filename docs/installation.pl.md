# Instalacja paczki

- **Zainstaluj paczkę**:

```shell
composer require x-one/przelewy24-bundle
```

Symfony Flex powinien automatycznie dodać nowe wpisy do plików `config/bundles.php`.

- **Ręcznie zaktualizuj plik config/routes.yaml**:

```
x_one_przelewy24_bundle:
  resource: "@XOnePrzelewy24Bundle/config/routes.yaml"
```

- **Utwórz encję dziedziczącą po Przelewy24Transaction**:

```php
<?php

declare(strict_types=1);

namespace App\Entity;

use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24Transaction;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Transaction extends Przelewy24Transaction
{
}
```

- **Utwórz plik config/packages/x_one_przelewy24.yaml**:

Wskaż na stworzoną encję i podaj dane do połączenia z API.

```
x_one_przelewy24:
  entities:
    transaction: App\Entity\Transaction
  api:
    merchant_id: '%env(int:PRZELEWY24_MERCHANT_ID)%'
    reports_key: '%env(string:PRZELEWY24_REPORTS_KEY)%'
    crc: '%env(string:PRZELEWY24_CRC)%'
    is_live: '%env(bool:PRZELEWY24_IS_LIVE)%'
```

- **Uzupełnij .env.local w poszczególnych środowiskach**:

```
PRZELEWY24_MERCHANT_ID=
PRZELEWY24_REPORTS_KEY=
PRZELEWY24_CRC=
PRZELEWY24_IS_LIVE=
```


## Development

Rozwijając paczkę możemy posłużyć się konfiguracją wskazującą na pliki lokalne:

```
"repositories": [
    {"type": "path", "url": "lib/x-one/przelewy24-bundle"}
],
```

Oraz:

```
composer require x-one/przelewy24-bundle dev-main
```

Aby Przelewy24 mogło uderzać na lokalne API, możemy posłużyć się ngrokiem:

```
ngrok http --domain=wasza-darmowa-domena.ngrok-free.app https://localhost:443
```

Wymaga to skonfigurowania aplikacji, pod którą rozwijana jest paczka, do darmowej domeny Ngroka LUB lokalnej modyfikacji kodu generującego URL statusu.

Jako lokalny hack (na datę pisania dokumentacji), w klasie `Przelewy24Client` można nadpisać:

```
protected function getUrlStatus(): string
{
    return 'https://wasza-darmowa-domena-free.app/_przelewy24/status-webhook';
}
```
