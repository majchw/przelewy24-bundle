# Installing the bundle

- **Install x-one/przelewy24-bundle via Composer**:

```shell
composer require x-one/przelewy24-bundle
```

Symfony Flex should automatically update `config/bundles.php`.

- **Update config/routes.yaml**:

```
x_one_przelewy24_bundle:
  resource: "@XOnePrzelewy24Bundle/config/routes.yaml"
```

- **Create an entity inheriting from Przelewy24Transaction**:

```php
<?php

declare(strict_types=1);

namespace App\Entity;

use XOne\Bundle\Przelewy24Bundle\Entity\Przelewy24Transaction;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Transaction extends Przelewy24Transaction
{
}
```

- **Create config/packages/x_one_przelewy24.yaml**:

Provide created entity's FQCN. We'll read Przelewy24 API keys from .env.

```
x_one_przelewy24:
  entities:
    transaction: App\Entity\Transaction
  api:
    merchant_id: '%env(int:PRZELEWY24_MERCHANT_ID)%'
    reports_key: '%env(string:PRZELEWY24_REPORTS_KEY)%'
    crc: '%env(string:PRZELEWY24_CRC)%'
    is_live: '%env(bool:PRZELEWY24_IS_LIVE)%'
```

- **Set the Przelewy24 API keys**:

Complete the variables in .env.local for local environments.

```
PRZELEWY24_MERCHANT_ID=
PRZELEWY24_REPORTS_KEY=
PRZELEWY24_CRC=
PRZELEWY24_IS_LIVE=
```


## Developing the bundle

For development, you can use a local configuration.

In an existing Symfony project create a top-level `lib` directory. The directory structure should mimic vendor.

- Update `composer.json`:

```
"repositories": [
    {"type": "path", "url": "lib/x-one/przelewy24-bundle"}
],
```

- Load the bundle:

```
composer require x-one/przelewy24-bundle dev-main
```

- Use ngrok to expose local API for Przelewy24 webhooks:

```
ngrok http --domain=your-free-domain.ngrok-free.app https://localhost:443
```

You can either configure the Symfony application to support `your-free-domain.ngrok-free.app`, or as a local hack you can overwrite `Przelewy24Client` status URL generator:

```
protected function getUrlStatus(): string
{
    return 'https://your-free-domain.ngrok-free.app/_przelewy24/status-webhook';
}
```
